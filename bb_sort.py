def bubble_sort(data: list) -> list:
    sorted_data = data.copy()
    n = len(data) - 1
    for i in range(len(data)):
        for j in range(n):
            if sorted_data[j] > sorted_data[j + 1]:
                sorted_data[j], sorted_data[j + 1] = sorted_data[j + 1], sorted_data[j]
    return sorted_data

    # while n > 0:
    #     for i in range(n):
    #         if data[i] > data[i + 1]:
    #             data[i], data[i + 1] = data[i + 1], data[i]
    #     n -= 1
    #     print(data)
    # return data


if __name__ == "__main__":
    data = list(map(int, input("Enter 10 number: ").split()))
    sort_data = bubble_sort(data)
    print("Bubble sort =", *sort_data, sep=" ")
