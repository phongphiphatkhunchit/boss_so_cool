import timeit


def recurcive(n):
    if n == 1:
        return 1
    if n < 1:
        return 0
    return recurcive(n - 1) + recurcive(n - 2)


def bottom_up(n):
    f0 = 0
    f1 = 1
    f2 = 1
    for i in range(2, n + 1):
        f2 = f1 + f0
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    n = int(input("Input number: "))
    print(recurcive(n))
    print(bottom_up(n))
