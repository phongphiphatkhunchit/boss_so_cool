def greedy(money, coin):
    many_coin = [0, 0, 0, 0, 0]
    for i in range(len(coin)):
        if money >= coin[i]:
            many_coin[i] = money // coin[i]
            money = money % coin[i]
    return many_coin, coin


def topdown(money, coin, lenght, min_coin):
    if min_coin[money - 1] != 100:
        return min_coin
    pre_min = [100]
    for i in range(lenght):
        pre_anw = [0 for j in range(lenght)]
        if money - coin[i] >= 0:
            pre_anw[i] += 1
            if money - coin[i] == 0:
                min_coin[money - 1] = pre_anw
                return min_coin
            min_coin = topdown(money - coin[i], coin, lenght, min_coin)
            pre_anw = [
                pre_anw[j] + min_coin[money - coin[i] - 1][j] for j in range(lenght)
            ]
            if sum(pre_min) > sum(pre_anw):
                pre_min = pre_anw
    min_coin[money - 1] = pre_min
    return min_coin


def bottom_up(money, coin):
    lentgh = len(coin)
    anw = list()
    for i in range(1, money + 1):
        pre_anw = [100]
        for j in range(lentgh):
            if i - coin[j] >= 0:
                min_coin = [0 for k in range(lentgh)]
                min_coin[j] += 1
                if i - coin[j] == 0:
                    pre_anw = min_coin
                    break
                min_coin = [
                    min_coin[k] + anw[i - coin[j] - 1][k] for k in range(lentgh)
                ]
                if sum(pre_anw) > sum(min_coin):
                    pre_anw = min_coin
        anw.append(pre_anw)
    return anw[money - 1]


if __name__ == "__main__":
    money = int(input("Enter : "))
    min_coin = [100 for i in range(money)]
    coin = [1, 2, 4, 5, 10]
    min_coin = topdown(money, coin, len(coin), min_coin)

    print("\nTop down result =", sum(min_coin[money - 1]))
    for i, j in zip(coin, min_coin[money - 1]):
        print(f"Coin {i} is {j}")

    min_coin = bottom_up(money, coin)

    print("\nBottom up result =", sum(min_coin))
    for i, j in zip(coin, min_coin):
        print(f"Coin {i} is {j}")

    min_coin, coin = greedy(money, coin[::-1])

    print("\nGreedy result =", sum(min_coin))
    for i, j in zip(coin, min_coin):
        print("Coin {} is {}".format(i, j))
