def selection_sort(data):
    for i in range(len(data) - 1, 0, -1):
        maxi = 0
        for j in range(1, i + 1):
            if data[j] > data[maxi]:
                maxi = j
        data[i], data[maxi] = data[maxi], data[i]
    return data


if __name__ == "__main__":
    data = [int(x) for x in input("Input number : ").split()]
    sort_data = selection_sort(data)
    print("Selection sort =", *sort_data, sep=" ")
